package com.nupco.jalo;

import com.nupco.constants.YsaperpreturnprocessConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class NupcosaperpreturnprocessManager extends GeneratedNupcosaperpreturnprocessManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( NupcosaperpreturnprocessManager.class.getName() );
	
	public static final NupcosaperpreturnprocessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (NupcosaperpreturnprocessManager) em.getExtension(YsaperpreturnprocessConstants.EXTENSIONNAME);
	}
	
}
