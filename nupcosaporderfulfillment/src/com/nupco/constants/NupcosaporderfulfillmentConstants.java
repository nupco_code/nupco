/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.nupco.constants;

/**
 * Global class for all Nupcosaporderfulfillment constants. You can add global constants for your extension into this class.
 */
public final class NupcosaporderfulfillmentConstants extends GeneratedNupcosaporderfulfillmentConstants
{
	@SuppressWarnings("javadoc")
	public static final String EXTENSIONNAME = "nupcosaporderfulfillment";

	@SuppressWarnings("javadoc")
	public static final String ORDER_PROCESS_NAME = "sap-order-process";

	private NupcosaporderfulfillmentConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
